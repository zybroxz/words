﻿using System;
using System.Linq;
using NUnit.Framework;

namespace Words.Test
{
    [TestFixture]
    public class WordServiceGetWordsFromHtmlShould
    {
        private IWordService _wordService;

        [SetUp]
        public void Setup()
        {
            _wordService = new WordService();
        }

        [Test]
        public void ThrowsExceptionWhenArgumentIsNull()
        {
            var html = string.Empty;
  
            Assert.Throws<ArgumentException>(() => _wordService.GetWordsFromHtml(html));
        }

        [Test]
        public void Returns30WordsWhen35WordsInHtml()
        {
            const string html = @"<html>  <head></head> <body> <p> this is some text question answer today Tomorrow</p>  <h1>tomorrow</h1>  <p>Monotonectally e-enable state of the art products via effective innovation. Intrinsicly aggregate magnetic human capital with excellent testing procedures. Credibly architect bricks-and-clicks meta-services rather than scalable synergy. Appropriately enable empowered e-markets.<p>   </body>  </html>";

            var result = _wordService.GetWordsFromHtml(html);

            Assert.AreEqual(30, result.Count());
        }

        [Test]
        public void Returns19WordsWhen19WordsInHtml()
        {
            const string html = @"<html>  <head></head> <body> <p> this is some text question answer today tomorrow</p>  <h1>tomorrow</h1>  <p>Monotonectally e-enable state of the art products via effective innovation. Intrinsicly aggregate magnetic human capital with <p>   </body>  </html>";

            var result = _wordService.GetWordsFromHtml(html);

            Assert.AreEqual(19, result.Count());
        }

        [Test]
        public void ReturnsAWeightOf2ForTheWordTomorrow()
        {
            const string html = @"<html>  <head></head> <body> <p> this is some text question answer today tomorrow</p>  <h1>Tomorrow</h1>  <p>Monotonectally e-enable state of the art products via effective innovation. Intrinsicly aggregate magnetic human capital with <p>   </body>  </html>";

            var result = _wordService.GetWordsFromHtml(html);

            Assert.AreEqual(2, result.FirstOrDefault(x => x.Text.ToLower() == "tomorrow").Weight);
        }
    }
}
