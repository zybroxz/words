using System.Collections.Generic;

namespace Words
{
    public interface IWordService
    {
        List<WordResult> GetWordsFromHtml(string html);
    }
}