﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Words.Controllers
{
    public class WordController : ApiController
    {
        private readonly IWordService _wordService;
        
        public WordController(IWordService wordService)
        {
            _wordService = wordService;
        }

        public HttpResponseMessage Get(string url)
        {
            if (string.IsNullOrEmpty(url))
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            if (!url.StartsWith("http://"))
                url = "http://" + url;

            string html;

            try
            {
                html = new WebClient().DownloadString(url);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }

            var grouped = _wordService.GetWordsFromHtml(html);

            return Request.CreateResponse(HttpStatusCode.OK, grouped);
        }

    }
}
