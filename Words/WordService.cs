using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace Words
{
    public class WordService : IWordService
    {
        public List<WordResult> GetWordsFromHtml(string html)
        {
            dynamic grouped = null;

            if (!String.IsNullOrEmpty(html))
            {
                var ap = new HtmlDocument();

                ap.LoadHtml(html);

                ap.DocumentNode.Descendants()
                    .Where(n => n.Name == "script" || n.Name == "style")
                    .ToList()
                    .ForEach(n => n.Remove());

                var body = ap.DocumentNode.SelectSingleNode("//body");

                var text = WebUtility.HtmlDecode(body.InnerText).ToLower();

                var words = Regex.Matches(text, @"[A-Za-z0-9]+");

                var wordList = (from object word in words select word.ToString()).ToList();

                grouped = wordList.Where(w => w.Length > 3)
                    .GroupBy(s => s)
                    .Select(g => new WordResult {Text = g.Key, Weight = g.Count()})
                    .OrderByDescending(y => y.Weight)
                    .Take(30)
                    .ToList();
            }
            else
            {
                throw new ArgumentException("url");
            }

            return (List<WordResult>) grouped;
        }
    }
}