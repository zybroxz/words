﻿namespace Words
{
    public class WordResult
    {
        public string Text { get; set; }
        public int Weight { get; set; }
    }
}